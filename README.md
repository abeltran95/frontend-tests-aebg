# BIMBONET #

![Tconecta-logo](https://blmrecargas.s3.amazonaws.com/TCONECTA150x150.png)

Construye una aplicación en Angular (9+) que busque perritos, de preferencia integrar el API de Giphy con las siguientes características:

* Hacer fork a este repositorioo
* Responsivo
* Componentes reutilizables
* Usar animaciones para que le den ganas al usuario de regresar
* Guardar búsquedas en localstorage
* Guardar favoritos en localstorage
* Subir propuesta como pull request

### Opcionalmente ###

* Usar ng-zorro para definir los estilos de los componentes